package Colecciones

import sc

data class Employer(
    val nom:String,
    val cognom:String,
    val dni:String,
    val adreça:String)
fun main() {
    val empleat= mutableMapOf<String,Employer>()
    val empleats=sc.nextLine().toInt()
    for(i in 0 until empleats){
        println("dni")
        val dni=sc.nextLine()
        println("Nom")
        val nom=sc.nextLine()
        println("Cognom")
        val cognom=sc.nextLine()
        println("Adreça")
        val adreça=sc.nextLine()
        empleat.put(dni,Employer(nom,cognom,dni,adreça))
    }
    var consulta=""
    while (true){
        consulta=sc.next()
        if(consulta.uppercase()=="END")break
        println("${empleat[consulta]?.nom} ${empleat[consulta]?.cognom} - ${empleat[consulta]?.dni}, ${empleat[consulta]?.adreça}")
    }
}