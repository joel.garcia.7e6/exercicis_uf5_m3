import java.util.*
val sc = Scanner(System.`in`)
fun main() {
    val señales= mutableMapOf<Int,String>()
    val numSeñales=sc.nextInt()
    for(i in 0 until numSeñales){
        val metro=sc.nextInt()
        val señal=sc.next()
        señales.put(metro,señal)
    }
    val numConsultas=sc.nextInt()
    for(i in 0 until numConsultas){
        val consulta=sc.nextInt()
        if(consulta in señales) println(señales[consulta])
        else println("No hi ha cartell")
    }
}