package Interficies


import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

@Serializable
data class Student(
    val name:String,
    val textGrade:String,
)


val ExOne = File("src/main/resources/Ex1.json")
fun main(){

    val file= ExOne.readLines()
    val student1 = Student("Joel", "A")
    val student2 = Student("Jordi", "F")
    val toJson1 = Json.encodeToString<Student>(student1)
    val toJson2 = Json.encodeToString<Student>(student2)
    ExOne.appendText("$toJson1\n")
    ExOne.appendText("$toJson2\n")

    println(student1)
    println(student2)
}
