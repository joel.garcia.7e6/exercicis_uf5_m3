package Interficies

import sc
import java.util.*


interface GymControlReader{
    fun nextId(): String
}

class GymControlManualReader() : GymControlReader {
    override fun nextId() = sc.next()
}

fun main() {
    val gymCMR = GymControlManualReader()
    val llistaidClients = mutableListOf<String>()

    for (id in 1..8){
        val id = gymCMR.nextId()

        if (id !in llistaidClients) {
            llistaidClients.add(id)
            println("$id ENTRADA")
        }
        else{
            llistaidClients.remove(id)
            println("$id SORTIDA")
        }
    }
}
