package Interficies

import sc


interface CarSensors{
    fun hiHaObstacle(direction: Direction) : Boolean
    fun avanzar(direction : Direction)
    fun aturar()
}

enum class Direction{
    FRONT,LEFT,RIGHT
}

class AutonomousCar(): CarSensors{
    override fun hiHaObstacle(direction: Direction): Boolean {
        TODO("Check if the following position is occupied or not.")
    }

    override fun avanzar(direction: Direction) {
        TODO("Go to following direction.")
    }

    override fun aturar() {
        TODO("Stop the car.")
    }

    fun ferSeguentAccio(times: Int){
        for (i in 1..times){
            if (!hiHaObstacle(Direction.FRONT)) avanzar(Direction.FRONT)
            else {
                if (!hiHaObstacle(Direction.RIGHT)) avanzar(Direction.RIGHT)
                else {
                    if (!hiHaObstacle(Direction.LEFT)) avanzar(Direction.LEFT)
                    else aturar()
                }
            }
        }
    }
}

fun main() {
    val auto = AutonomousCar()

    auto.ferSeguentAccio(5)
}
