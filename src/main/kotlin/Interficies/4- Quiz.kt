package Interficies

import sc

class Preguntas {
    companion object{
        val questionList = listOf<Question>(
            OpcioMultiple("Quin dels següents nùmeros no es primer?","A.2","B.25","C.23","D.11",'B'),
            OpcioMultiple("Quin dels següents no era un apostol de Jesus?","A.José","B.Judas","C.Iscariote","D.Juan",'A'),
            RespostaOberta("Cuantes llunes té Mart?","2"),
            RespostaOberta("Quin es el mamifer mes gran del mon?","BALENA")
        )
    }
}
var preguntesResoltes: Int = 0

interface Question{
    fun escriuPregunta(num: Int)
    fun preguntesResoltes()
}

class RespostaOberta(private val enunciat: String, val resposta: String): Question{
    private var resolt: Boolean = false

    override fun escriuPregunta(num: Int) {
        println("\u001B[7m Pregunta ${num + 1}: \u001B[0m")
        println(this.enunciat)
    }

    override fun preguntesResoltes() {
        do {
            println("Escriu la resposta correcte:")
            val respostaUsuari = sc.nextLine().uppercase()

            if (this.resposta == respostaUsuari) {
                this.resolt = true
                preguntesResoltes++
                println("\u001B[92m $respostaUsuari es la resposta correcte!\u001B[0m ")
            }
            else println("\u001B[91m Aquesta no es la resposta correcte...\u001B[0m")
        } while (!this.resolt && respostaUsuari != "CANCEL")
    }
}

class OpcioMultiple (private val enunciat: String, val option1 : String, val option2: String, val option3: String, val option4: String, val resposta: Char): Question{
    private var resolt: Boolean = false
    override fun escriuPregunta(num: Int) {
        println("\u001B[7m Pregunta ${num+1}: \u001B[0m ")
        println(this.enunciat)
        println("·${this.option1}\n·${this.option2}\n·${this.option3}\n·${this.option4}")
    }

    override fun preguntesResoltes() {
        do {
            println("Escriu la resposta correcte:")
            val respostaUsuari = sc.next().uppercase().single()

            if (this.resposta == respostaUsuari) {
                this.resolt = true
                preguntesResoltes++
                println("\u001B[92m $respostaUsuari es la resposta correcte!\u001B[0m ")
            }
            else println("\u001B[91m Aquesta no es la resposta correcte...\u001B[0m ")
        } while (!this.resolt)
    }
}

fun main(){
    val questionsGet = Preguntas.questionList
    for (i in questionsGet.indices){
        val question = questionsGet[i]
        question.escriuPregunta(i)
        question.preguntesResoltes()
    }

    println("Has resolt $preguntesResoltes de ${questionsGet.size} preguntes!")
}
