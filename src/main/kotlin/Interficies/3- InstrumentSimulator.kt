package Interficies

abstract class Instrument{
    abstract fun ferSoroll(times:Int):String
}
class Triangle(val resonancia:Int):Instrument(){
    override fun ferSoroll(times: Int):String {
        var so = ""
        repeat(times){
            when(resonancia){
                1 -> so+= "TINC "
                2 -> so+="TIINC "
                3 -> so+="TIIINC "
                4 -> so+="TIIIINC "
                5 -> so+="TIIIIINC "
            }
        }
        return so
    }
}
class Tambor(val to:String):Instrument(){
    override fun ferSoroll(times: Int):String {
        var so = ""
        repeat(times){
            when(to){
                "A" -> so += "TAAAM "
                "O" -> so += "TOOOM "
                "U" -> so += "TUUUM "
            }
        }
        return  so
    }
}
fun main(){
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Tambor("A"),
        Tambor("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}
private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        println(instrument.ferSoroll(2)) // plays 2 times the sound
    }
}